//
//  TestDemoTests.swift
//  TestDemoTests
//
//  Created by Rao Mudassar on 20/01/2022.
//

import XCTest
@testable import TestDemo

class TestDemoTests: XCTestCase {

    var squirtle:Items?

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        self.setUp()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        squirtle = nil
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    override func setUp() {
            super.setUp()
            // Put setup code here. This method is called before the invocation of each test method in         the class.
            squirtle = Items(id: "0", name: "Mens Cotton Jacket", image: "", price: "55.99")

        }

}
